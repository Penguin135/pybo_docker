```
docker-compose up
```
- Django, nginx 서비스를 실행할 수 있습니다.
- docker compose 내부에서 docker volume을 생성하고 static 파일들을 마운트하여 사용합니다.

```
docker-compose down -v
```
- Django, nginx 서비스를 종료합니다.
- docker compose 내부에서 생성한 docker volume을 -v 옵션을 주어 자동으로 제거합니다.
